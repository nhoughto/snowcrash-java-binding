# snowcrash Java Binding

Java binding for [snowcrash](http://github.com/apiaryio/snowcrash).

## What's it for?
- Convert [API Blueprint](http://apiblueprint.org/) files in Markdown format into JSON or YAML AST files using Java
- Load API Blueprint files in Markdown format into POJOs
- Load AST files in JSON or YAML format into POJOs

Why? Use them with something like the [Java API Blueprint Mock Server](https://bitbucket.org/outofcoffee/api-blueprint-mockserver).

## Writing mocks
We support the [API Blueprint](http://apiblueprint.org/) format e.g.

    # My API
    ## GET /message
    + Response 200 (text/plain)

            Hello World!

## How do I get it?
### Prerequisites
- Java 6 or higher
- [snowcrash](http://github.com/apiaryio/snowcrash) installed

### Instructions
Set the *SNOWCRASH_HOME* environment variable:

    SNOWCRASH_HOME="/path/to/snowcrash/dir"

_NOTE: If you installed snowcrash using Homebrew, you can try: **SNOWCRASH_HOME="/usr/local/bin"**_

### Method 1: Get the JAR as a dependency
In your Maven or Gradle build file, add the repository as follows:

Maven (in pom.xml):
    
    <repositories>
      <repository>
        <id>snowcrash-java-binding</id>
        <url>https://bitbucket.org/outofcoffee/snowcrash-java-binding/raw/HEAD/repo/</url>
       </repository>
    </repositories>

Gradle (in build.gradle):

    repositories {
        // your other repositories here, e.g. mavenCentral()
        // ...
        
        // repository for snowcrash Java Binding
        maven {
            url "https://bitbucket.org/outofcoffee/snowcrash-java-binding/raw/HEAD/repo/"
        }
    }

### Method 2: Build the JAR from source
Clone this repository:

    git clone https://bitbucket.org/outofcoffee/snowcrash-java-binding.git snowcrash-java

_NOTE: If you don't have Git or Subversion, you can also download the repository as a ZIP file._

Change directory to the downloaded repository:

    cd snowcrash-java

Build using Gradle wrapper:

    sh ./gradlew clean publishToMavenLocal

The library is now available in your local Maven repository ($HOME/.m2/repository). You can depend on it in your projects as follows:

### Add it to your project
Maven (in pom.xml):

    <dependency>
        <groupId>com.gatehill.apib</groupId>
        <artifactId>apib-parser</artifactId>
        <version>0.2.0</version>
    </dependency>

Gradle (in build.gradle):

    compile 'com.gatehill.apib:apib-parser:0.2.0'

## How do I use it?
### Using the parser to convert API Blueprints in Markdown format to AST

Instantiate an instance of *SnowcrashBlueprintParserServiceImpl*:

    final BlueprintParserService parser = new SnowcrashBlueprintParserServiceImpl();

    // configure parser (e.g. set snowcrash directory if not using an environment variable)
    config = new ParserConfiguration();

    // use the parser ...

For a full working example see *src/test/java/.../SnowcrashBlueprintParserServiceTest.java*

    // blueprint file
    final File blueprintFile = new File("/path/to/api-blueprint.md");

    // convert to AST
    final ParsingResult<File> result = parser.convertToAstFile(config, blueprintFile, BlueprintParser.OutputFormat.JSON);

    // check result is OK
    assert AstResult.ErrorCode.NoError.getErrorCode() == result.getError().getCode();

    // assert file exists
    assert result.getAst().exists();

    // use the JSON AST file ...

For an example API Blueprint file see *src/test/resources/api1.md*

### Using the parser to read AST files to a POJO model

Instantiate an instance of *JsonAstParserServiceImpl*:

    final AstParserService service = new JsonAstParserServiceImpl();

    // use the parser ...

For a full working example see *src/test/java/.../JsonAstParserServiceTest.java*

    // JSON format AST file
    final File blueprint = new File("/path/to/ast-file.json");

    // load AST into model
    final AstBlueprint astBlueprint = service.fromAst(blueprint, AstFormat.JSON);

    // assert model is populated
    assert astBlueprint.getName().equals("My API");

    // use the model ...

For an example AST file see *src/test/resources/api1.json*

## Architecture
We use a number of frameworks and libraries to make this project possible:

- *slf4j* and *logback* for logging
- *Gson* for JSON deserialisation
- *snakeyaml* for YAML deserialisation
- Apache commons-io and commons-lang
- and many more...

For testing:

- *JUnit* for unit testing

## Testing

### Continuous Integration
We use [drone.io](https://drone.io/bitbucket.org/outofcoffee/snowcrash-java-binding)

[![Build Status](https://drone.io/bitbucket.org/outofcoffee/snowcrash-java-binding/status.png)](https://drone.io/bitbucket.org/outofcoffee/snowcrash-java-binding/latest)

### Running tests
Run the following setup script before the unit tests:

    cd support/
    source ./download-parsers.sh

This downloads the parsers and sets some environment variables that the unit tests expect.

To run tests:

    sh ./gradlew clean test
