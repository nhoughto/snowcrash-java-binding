#!/bin/bash

set -e

# one entry per parser version
PARSER_VERSIONS=( v0.8.1 v0.15.0 )
ENVARS=( TEST_SNOWCRASH1_HOME TEST_SNOWCRASH2_HOME )

# other vars
LOCAL_WORKING_DIR="/tmp"
PARSER_DIR="${LOCAL_WORKING_DIR}/snowcrash"
LOCAL_REPO_NAME="snowcrash.git"

echo ""
echo "Working dir: ${LOCAL_WORKING_DIR}"
cd ${LOCAL_WORKING_DIR}

# remove existing repo
if [[ -d "${LOCAL_REPO_NAME}" ]]; then
  echo "Removing existing ${LOCAL_REPO_NAME}"
  rm -rf ${LOCAL_REPO_NAME}
fi

# remove existing parsers
if [[ -d "${PARSER_DIR}" ]]; then
  echo "Removing existing ${PARSER_DIR}"
  rm -r ${PARSER_DIR}
fi

# download snowcrash
git clone --recursive git://github.com/apiaryio/snowcrash.git ${LOCAL_REPO_NAME}
cd ${LOCAL_REPO_NAME}

for (( i = 0 ; i < ${#PARSER_VERSIONS[@]} ; i++ )) do
    PARSER_VERSION=${PARSER_VERSIONS[$i]}
    ENVAR=${ENVARS[$i]}

    echo ""
    echo "----------------------------------------"
    echo "Version: $PARSER_VERSION"
    echo "Environment variable: $ENVAR"
    echo "----------------------------------------"

    # check out version
    git checkout ${PARSER_VERSION}
    git submodule update --init --recursive

    # build
    ./configure && make clean snowcrash

    # create empty binary dir
    VERSION_HOME=${PARSER_DIR}/${PARSER_VERSION}

    if [[ -d ${VERSION_HOME} ]]; then
        rm -r ${VERSION_HOME}
    fi
    mkdir -p ${VERSION_HOME}
    VERSION_HOME=$( cd ${VERSION_HOME} && pwd )

    # keep binary
    cp ./bin/snowcrash ${VERSION_HOME}/snowcrash

    # set env variable
    export ${ENVAR}=${VERSION_HOME}
    echo ""
    echo "Set ${ENVAR}=${VERSION_HOME}"

    # cleanup
    make clean
done

echo ""
echo "----------------------------------------"
echo "Set environment variables:"

for (( i = 0 ; i < ${#PARSER_VERSIONS[@]} ; i++ )) do
    echo "${ENVARS[$i]}"
done

echo "----------------------------------------"
echo ""
echo "Done."
echo ""
