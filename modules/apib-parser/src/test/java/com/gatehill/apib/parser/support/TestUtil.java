package com.gatehill.apib.parser.support;

import com.gatehill.apib.parser.model.ParserConfiguration;
import org.apache.commons.lang.StringUtils;

import java.io.File;

/**
 * @author pete
 */
public class TestUtil {
    /**
     * The name of the environment variable specifying the V1 parser home directory.
     */
    public static final String ENV_SNOWCRASH1_HOME = "TEST_SNOWCRASH1_HOME";

    /**
     * The name of the environment variable specifying the V2 parser home directory.
     */
    public static final String ENV_SNOWCRASH2_HOME = "TEST_SNOWCRASH2_HOME";

    /**
     * Build configuration using the parser in the location specified by the
     * environment variable <code>envVarName</code>.
     *
     * @param envVarName the name of the environment variable specifying the parser home directory
     * @return a ParserConfiguration with the parser directory set
     */
    public static ParserConfiguration buildConfigFromEnvironmentVariable(String envVarName) {
        final String snowcrashHome = System.getenv(envVarName);
        if (StringUtils.isBlank(snowcrashHome)) {
            throw new RuntimeException("Missing environment variable: " + envVarName);
        }

        final File parserDir = new File(snowcrashHome);
        if (!parserDir.exists()) {
            throw new RuntimeException("Unable to find parser at: " + parserDir.getAbsolutePath());
        }

        final ParserConfiguration config = new ParserConfiguration();
        config.setParserHome(parserDir);

        return config;
    }
}
