package com.gatehill.apib.parser.util;

import com.gatehill.apib.parser.model.result.AstResult;
import com.gatehill.apib.parser.model.result.SourceAnnotation;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Tests for {@link ResultUtil}.
 *
 * @author pcornish
 */
public class ResultUtilTest {

    public static final String WARNING_MESSAGE = "missing response HTTP status code, assuming 'Response 200'";

    /**
     * Verify that an 'OK' result can be parsed, with no warnings and an error code of 0.
     *
     * @throws IOException
     */
    @Test
    public void testGetParserResult_OK() throws IOException {
        // test data
        final File parserResult = new File(ResultUtilTest.class.getResource("/result_ok.txt").getPath());

        // call
        final AstResult actual = ResultUtil.getParserResult(FileUtils.readFileToString(parserResult));

        // assert output
        Assert.assertNotNull(actual);

        // assert error
        Assert.assertNotNull(actual.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), actual.getError().getCode());
        Assert.assertNull(actual.getError().getMessage());
        Assert.assertNull(actual.getError().getLocation());

        // assert warnings
        Assert.assertNotNull(actual.getWarnings());
        Assert.assertEquals(0, actual.getWarnings().length);
    }

    /**
     * Verify that an error result can be parsed, with no warnings and an error code of 2.
     *
     * @throws IOException
     */
    @Test
    public void testGetParserResult_Error() throws IOException {
        // test data
        final File parserResult = new File(ResultUtilTest.class.getResource("/result_error.txt").getPath());

        // call
        final AstResult actual = ResultUtil.getParserResult(FileUtils.readFileToString(parserResult));

        // assert output
        Assert.assertNotNull(actual);

        // assert error
        Assert.assertNotNull(actual.getError());
        Assert.assertEquals(AstResult.ErrorCode.BusinessError.getErrorCode(), actual.getError().getCode());
        Assert.assertEquals("unexpected header block, expected a group, resource or an action definition, e.g. '# Group <name>', '# <resource name> [<URI>]' or '# <HTTP method> <URI>'",
                actual.getError().getMessage());

        Assert.assertNotNull(actual.getError().getLocation());
        Assert.assertEquals(826, actual.getError().getLocation().getLocation());
        Assert.assertEquals(8, actual.getError().getLocation().getLength());

        // assert warnings
        Assert.assertNotNull(actual.getWarnings());
        Assert.assertEquals(0, actual.getWarnings().length);
    }

    /**
     * Verify that an 'OK' result can be parsed, with 3 warnings and an error code of 0.
     *
     * @throws IOException
     */
    @Test
    public void testGetParserResult_Warnings() throws IOException {
        // test data
        final File parserResult = new File(ResultUtilTest.class.getResource("/result_warnings.txt").getPath());

        // call
        final AstResult actual = ResultUtil.getParserResult(FileUtils.readFileToString(parserResult));

        // assert output
        Assert.assertNotNull(actual);

        // assert error
        Assert.assertNotNull(actual.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), actual.getError().getCode());
        Assert.assertNull(actual.getError().getMessage());
        Assert.assertNull(actual.getError().getLocation());

        // assert warnings
        Assert.assertNotNull(actual.getWarnings());
        Assert.assertEquals(3, actual.getWarnings().length);

        final SourceAnnotation warning1 = actual.getWarnings()[0];
        Assert.assertNotNull(warning1);
        Assert.assertEquals(WARNING_MESSAGE, warning1.getMessage());
        Assert.assertEquals(AstResult.WarningCode.EmptyDefinitionWarning.getWarningCode(), warning1.getCode());
        Assert.assertNotNull(warning1.getLocation());
        Assert.assertEquals(641, warning1.getLocation().getLocation());
        Assert.assertEquals(34, warning1.getLocation().getLength());

        final SourceAnnotation warning2 = actual.getWarnings()[1];
        Assert.assertNotNull(warning2);
        Assert.assertEquals(WARNING_MESSAGE, warning2.getMessage());
        Assert.assertEquals(AstResult.WarningCode.IgnoringWarning.getWarningCode(), warning2.getCode());
        Assert.assertNotNull(warning2.getLocation());
        Assert.assertEquals(642, warning2.getLocation().getLocation());
        Assert.assertEquals(35, warning2.getLocation().getLength());

        final SourceAnnotation warning3 = actual.getWarnings()[2];
        Assert.assertNotNull(warning3);
        Assert.assertEquals(WARNING_MESSAGE, warning3.getMessage());
        Assert.assertEquals(AstResult.WarningCode.RedefinitionWarning.getWarningCode(), warning3.getCode());
        Assert.assertNotNull(warning3.getLocation());
        Assert.assertEquals(643, warning3.getLocation().getLocation());
        Assert.assertEquals(36, warning3.getLocation().getLength());
    }
}
