package com.gatehill.apib.parser.model.ast.v2;

import com.gatehill.apib.parser.model.Blueprint;

import java.util.List;

/**
 * Created by pete on 18/02/2014.
 * <p/>
 * <p>
 * See https://github.com/apiaryio/api-blueprint-ast
 * </p>
 * <p>
 * See https://github.com/apiaryio/snowcrash/blob/master/src/Blueprint.h
 * </p>
 * <p>
 * See https://github.com/apiaryio/api-blueprint/blob/master/API%20Blueprint%20Specification.md
 * </p>
 *
 * @author pcornish
 */
public class AstBlueprint extends AstBase implements Blueprint {
    private String _version;

    /**
     * Metadata
     */
    private List<AstNameValueType> metadata;

    /**
     * The set of API Resource Groups
     */
    private List<AstResourceGroup> resourceGroups;

    public String get_version() {
        return _version;
    }

    public void set_version(String _version) {
        this._version = _version;
    }

    public List<AstNameValueType> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<AstNameValueType> metadata) {
        this.metadata = metadata;
    }

    public List<AstResourceGroup> getResourceGroups() {
        return resourceGroups;
    }

    public void setResourceGroups(List<AstResourceGroup> resourceGroups) {
        this.resourceGroups = resourceGroups;
    }
}
