package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.exception.ParserException;
import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ParserConfiguration;
import com.gatehill.apib.parser.model.ParsingResult;
import com.gatehill.apib.parser.model.result.AstResult;
import com.gatehill.apib.parser.util.ResultUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Converts an API Blueprint Markdown file into an AST, with a given format.
 * <p/>
 * This implementation expects the 'snowcrash' tool to be installed at either
 * the location specified by the environment variable 'SNOWCRASH_HOME' or the
 * value of {@link com.gatehill.apib.parser.model.ParserConfiguration#getParserHome()}.
 *
 * @author pcornish
 */
public class SnowcrashBlueprintParserServiceImpl implements BlueprintParserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SnowcrashBlueprintParserServiceImpl.class);

    /**
     * Name of the environment variable used to locate the snowcrash parser if
     * {@link com.gatehill.apib.parser.model.ParserConfiguration#getParserHome()} is <code>null</code>.
     */
    public static final String ENV_SNOWCRASH_HOME = "SNOWCRASH_HOME";

    /**
     * {@inheritDoc}
     */
    @Override
    public ParsingResult<File> convertToAstFile(ParserConfiguration config, File blueprintFile, AstFormat outputFormat) {
        try {
            LOGGER.debug("Parsing blueprint file: {}", blueprintFile);

            File parserHome = null;

            if (null != config.getParserHome()) {
                LOGGER.debug("Using parser home specified in configuration: {}", config.getParserHome());
                parserHome = config.getParserHome();

            } else {
                // check for environment variable
                final String snowcrashHome = System.getenv(ENV_SNOWCRASH_HOME);

                if (StringUtils.isNotBlank(snowcrashHome)) {
                    LOGGER.debug("Using {} environment variable: {}", ENV_SNOWCRASH_HOME, snowcrashHome);
                    parserHome = new File(snowcrashHome);
                }
            }

            if (null != parserHome) {
                if (!parserHome.isDirectory()) {
                    throw new ParserException("Execution directory does not exist: " + parserHome);

                } else if (!parserHome.canRead()) {
                    throw new ParserException("Execution directory is not readable: " + parserHome);
                }

            } else {
                LOGGER.warn("No parser home directory specified");
            }

            final ParsingResult<File> result = executeParser(parserHome, blueprintFile, outputFormat);
            LOGGER.debug("AST file: {}", result.getAst().getAbsolutePath());
            return result;

        } catch (Exception e) {
            throw new ParserException("Error parsing blueprint file to AST file: " + blueprintFile, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParsingResult<String> convertToAstString(ParserConfiguration config, File blueprintFile, AstFormat outputFormat) {
        try {
            final ParsingResult<File> result = convertToAstFile(config, blueprintFile, outputFormat);

            // read file to string and clean up file
            final String ast = FileUtils.readFileToString(result.getAst());
            LOGGER.trace("AST: {}", ast);
            FileUtils.deleteQuietly(result.getAst());

            return new ParsingResult<String>(result.getResult(), ast);

        } catch (IOException e) {
            throw new ParserException("Error converting blueprint file to string: " + blueprintFile, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParsingResult<InputStream> convertToAstStream(ParserConfiguration config, File blueprintFile, AstFormat outputFormat) {
        try {
            final ParsingResult<File> result = convertToAstFile(config, blueprintFile, outputFormat);

            // open file as stream
            final FileInputStream ast = FileUtils.openInputStream(result.getAst());
            LOGGER.trace("AST stream opened");

            return new ParsingResult<InputStream>(result.getResult(), ast);

        } catch (IOException e) {
            throw new ParserException("Error converting blueprint file to stream: " + blueprintFile, e);
        }
    }

    /**
     * Invoke the snowcrash parser.
     *
     * @param execDir
     * @param blueprintFile
     * @param outputFormat
     * @return
     * @throws IOException
     */
    private ParsingResult<File> executeParser(File execDir, File blueprintFile, AstFormat outputFormat)
            throws IOException, InterruptedException {

        final ParsingResult<File> result = new ParsingResult<File>();

        final String astFormat = (outputFormat == AstFormat.JSON ? "json" : "yaml");
        final File astFile = File.createTempFile(blueprintFile.getName(), "." + astFormat);

        String snowcrashCommand;
        if (null != execDir) {
            snowcrashCommand = "./snowcrash";
        } else {
            snowcrashCommand = "snowcrash";
        }

        final String command = snowcrashCommand
                + " --format " + astFormat
                + " --output " + astFile.getAbsolutePath()
                + " " + blueprintFile.getAbsolutePath();

        LOGGER.trace("Executing command: {}", command);
        final Process proc = Runtime.getRuntime().exec(command, null, execDir);
        final int exitCode = proc.waitFor();

        // capture and print output
        String output = IOUtils.toString(proc.getInputStream());
        if (null == output) {
            output = "";
        }

        final String stderr = IOUtils.toString(proc.getErrorStream());
        if (StringUtils.isNotBlank(stderr)) {
            output += "\r\n" + stderr;
        }
        LOGGER.debug("Parser output: {}", output.trim());

        final AstResult parserResult = ResultUtil.getParserResult(output);
        result.setResult(parserResult);
        LOGGER.debug("Parser result: {}", parserResult);

        // check exit code
        if (0 != exitCode) {
            throw new ParserException("Parser returned with a non zero exit code: " + exitCode);
        }

        // check AST created
        if (astFile.exists()) {
            LOGGER.debug("AST file sized {} bytes created at: {}", astFile.length(), astFile.getAbsolutePath());
        } else {
            throw new ParserException("AST file not created at: " + astFile.getAbsolutePath());
        }

        result.setAst(astFile);
        return result;
    }
}
