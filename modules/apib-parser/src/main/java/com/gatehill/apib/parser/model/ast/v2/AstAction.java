package com.gatehill.apib.parser.model.ast.v2;

import java.util.List;

/**
 * @author pcornish
 */
public class AstAction extends AstBase {
    /**
     * HTTP method
     */
    private String method;

    /**
     * Action-specfic Parameters
     */
    private List<AstParameter> parameters;

    /**
     * Action-specific HTTP headers
     */
    private List<AstNameValueType> headers;

    /**
     * Transactions examples
     */
    private List<AstTransactionExample> examples;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<AstParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<AstParameter> parameters) {
        this.parameters = parameters;
    }

    public List<AstNameValueType> getHeaders() {
        return headers;
    }

    public void setHeaders(List<AstNameValueType> headers) {
        this.headers = headers;
    }

    public List<AstTransactionExample> getExamples() {
        return examples;
    }

    public void setExamples(List<AstTransactionExample> examples) {
        this.examples = examples;
    }
}
