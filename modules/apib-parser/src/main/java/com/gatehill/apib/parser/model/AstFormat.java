package com.gatehill.apib.parser.model;

/**
 * @author pcornish
 */
public enum AstFormat {
    YAML,
    JSON
}
