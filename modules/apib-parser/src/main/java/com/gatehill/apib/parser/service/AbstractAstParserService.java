package com.gatehill.apib.parser.service;

import com.gatehill.apib.parser.exception.ParserException;
import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.Blueprint;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Build a {@link com.gatehill.apib.parser.model.Blueprint} from an API Blueprint AST file.
 *
 * @author pcornish
 * @see AbstractYamlAstParserService
 * @see AbstractJsonAstParserService
 */
public abstract class AbstractAstParserService<T extends Blueprint> implements AstParserService<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAstParserService.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public T fromAst(File astFile, AstFormat inputFormat) {
        LOGGER.info("Parsing {} format AST file {}", inputFormat, astFile);

        try {
            return fromAst(FileUtils.openInputStream(astFile), inputFormat);
        } catch (IOException e) {
            throw new ParserException(String.format("Error parsing %s format AST file: %s", inputFormat, astFile), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T fromAst(String ast, AstFormat inputFormat) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Parsing {} format AST string: {}", inputFormat, ast);
        } else {
            LOGGER.info("Parsing {} format AST string", inputFormat);
        }

        try {
            return fromAst(IOUtils.toInputStream(ast), inputFormat);
        } catch (Exception e) {
            throw new ParserException(String.format("Error parsing %s format AST string", inputFormat), e);
        }
    }

    /**
     * @return the model Class to which the AST should be deserialised
     */
    protected abstract Class<T> getTargetModelClass();
}
