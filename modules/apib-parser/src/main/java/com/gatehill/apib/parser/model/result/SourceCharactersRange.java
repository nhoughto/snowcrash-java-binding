package com.gatehill.apib.parser.model.result;

/**
 * A character range within the source code string.
 * <p/>
 * See https://github.com/apiaryio/snowcrash/blob/master/src/SourceAnnotation.h
 *
 * @author pcornish
 */
public class SourceCharactersRange {
    private int location;
    private int length;

    public SourceCharactersRange(int location, int length) {
        this.location = location;
        this.length = length;
    }

    @Override
    public String toString() {
        return "SourceCharactersRange{" +
                "location=" + location +
                ", length=" + length +
                '}';
    }

    public int getLocation() {
        return location;
    }

    public int getLength() {
        return length;
    }
}
