package com.gatehill.apib.parser.model.ast.v1;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author pcornish
 */
public class AstParameter extends AstBase {
    /**
     * Type
     */
    private String type;

    /**
     * Required flag
     */
    private boolean required;

    /**
     * Default Value, applicable only when `required == false`
     */
    @SerializedName("default")
    private String defaultValue;

    /**
     * Example Value
     */
    private String example;

    /**
     * Enumeration of possible values
     */
    private List<String> values;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDefault() {
        return defaultValue;
    }

    public void setDefault(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String exampleValue) {
        this.example = exampleValue;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
