package com.gatehill.apib.parser.service.v2;

import com.gatehill.apib.parser.model.ast.v2.AstBlueprint;
import com.gatehill.apib.parser.service.AbstractYamlAstParserService;

/**
 * Build a V1 Blueprint {@link com.gatehill.apib.parser.model.ast.v1.AstBlueprint} from a YAML format API
 * Blueprint AST file.
 *
 * @author pete, fieldju
 * @see com.gatehill.apib.parser.service.v2.JsonAstParserServiceImpl
 */
public class YamlAstParserServiceImpl extends AbstractYamlAstParserService<AstBlueprint> {
    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<AstBlueprint> getTargetModelClass() {
        return AstBlueprint.class;
    }
}
