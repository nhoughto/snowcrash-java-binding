package com.gatehill.apib.parser.model.result;

import java.util.Arrays;

/**
 * A parsing result report.
 * <p/>
 * Result of a source data parsing operation.
 * Composed of one error source annotation and a set of warning source annotations.
 * <p/>
 * See https://github.com/apiaryio/snowcrash/blob/master/src/SourceAnnotation.h
 *
 * @author pcornish
 */
public class AstResult {
    /**
     * Error codes
     */
    public enum ErrorCode {
        NoError(0),
        ApplicationError(1),
        BusinessError(2),
        SymbolError(3);

        private final int errorCode;

        ErrorCode(int errorCode) {
            this.errorCode = errorCode;
        }

        public int getErrorCode() {
            return errorCode;
        }
    }

    /**
     * Warning codes
     */
    public enum WarningCode {
        NoWarning(0),
        APINameWarning(1),
        DuplicateWarning(2),
        FormattingWarning(3),
        RedefinitionWarning(4),
        IgnoringWarning(5),
        EmptyDefinitionWarning(6),
        NotEmptyDefinitionWarning(7),
        LogicalErrorWarning(8),
        DeprecatedWarning(9),
        IndentationWarning(10),
        AmbiguityWarning(11);

        private final int warningCode;

        WarningCode(int warningCode) {
            this.warningCode = warningCode;
        }

        public int getWarningCode() {
            return warningCode;
        }
    }

    /**
     * Result error source annotation
     */
    private SourceAnnotation error;

    /**
     * Result warning source annotations
     */
    private SourceAnnotation[] warnings;

    public AstResult() {
    }

    public AstResult(SourceAnnotation error, SourceAnnotation[] warnings) {
        this.error = error;
        this.warnings = warnings;
    }

    @Override
    public String toString() {
        return "AstResult{" +
                "error=" + error +
                ", warnings=" + Arrays.toString(warnings) +
                '}';
    }

    public void setError(SourceAnnotation error) {
        this.error = error;
    }

    public void setWarnings(SourceAnnotation[] warnings) {
        this.warnings = warnings;
    }

    public SourceAnnotation[] getWarnings() {
        return warnings;
    }

    public SourceAnnotation getError() {
        return error;
    }
}
