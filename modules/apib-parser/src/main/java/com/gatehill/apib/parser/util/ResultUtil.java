package com.gatehill.apib.parser.util;

import com.gatehill.apib.parser.exception.ParserException;
import com.gatehill.apib.parser.model.result.AstResult;
import com.gatehill.apib.parser.model.result.SourceAnnotation;
import com.gatehill.apib.parser.model.result.SourceCharactersRange;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Builds a {@link com.gatehill.apib.parser.model.ParsingResult} from parser output.
 * <p/>
 * Example:
 * <pre>
 *     OK.
 *     warning: (6)  missing response HTTP status code, assuming 'Response 200' :641:34
 * </pre>
 *
 * @author pcornish
 */
public class ResultUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultUtil.class);

    private static final String RESULT_OK = "OK.";
    private static final String RESULT_ERROR = "error:";
    private static final String RESULT_WARNING = "warning:";

    /**
     * Example:
     * <pre>
     *     (6) message here
     * </pre>
     */
    private static final Pattern RESULT_CODE_PATTERN = Pattern.compile("^\\(([0-9]+)\\)(.*)");

    /**
     * Example:
     * <pre>
     *     message here :123:45
     * </pre>
     */
    private static final Pattern DETAIL_PATTERN = Pattern.compile("(.+):([0-9]+):([0-9]+)");

    /**
     * Build a {@link com.gatehill.apib.parser.model.ParsingResult} from the given parser output.
     *
     * @param output
     * @return
     */
    public static AstResult getParserResult(String output) {
        if (null == output) {
            LOGGER.warn("Parser result was null");
            return null;
        }

        final AstResult astResult = new AstResult();
        final List<SourceAnnotation> warnings = new ArrayList<SourceAnnotation>();

        // TODO should this use the LINE_SEPARATOR_UNIX instead of the system separator?
        for (String line : StringUtils.split(output, IOUtils.LINE_SEPARATOR)) {
            line = line.trim();
            if (0 == line.length()) {
                continue;
            }
            LOGGER.trace("Attempt to parse result line: {}", line);

            if (RESULT_OK.equals(line)) {
                astResult.setError(new SourceAnnotation(AstResult.ErrorCode.NoError.getErrorCode(), null, null));

            } else if (line.startsWith(RESULT_ERROR)) {
                line = line.substring(RESULT_ERROR.length() + 1).trim();
                astResult.setError(getSourceAnnotation(line));

            } else if (line.startsWith(RESULT_WARNING)) {
                line = line.substring(RESULT_WARNING.length() + 1).trim();
                warnings.add(getSourceAnnotation(line));

            } else {
                LOGGER.warn("Result line did not match any expected format: {}" + line);
            }
        }

        astResult.setWarnings(warnings.toArray(new SourceAnnotation[warnings.size()]));

        return astResult;
    }

    /**
     * Attempt to get source annotation information from the given line.
     *
     * @param line
     * @return
     */
    private static SourceAnnotation getSourceAnnotation(String line) {
        final Matcher matcher = RESULT_CODE_PATTERN.matcher(line);
        if (matcher.matches()) {
            /*
             * Extract the result code.
             */
            final int resultCode;
            final String strResultCode = matcher.group(1);
            if (StringUtils.isNumeric(strResultCode)) {
                resultCode = Integer.parseInt(strResultCode);
            } else {
                throw new ParserException(String.format("Result code '%s' was not numeric!", strResultCode));
            }

            /*
             * Extract the message and build SourceAnnotation.
             */
            final String detail = matcher.group(2);
            final String message;
            SourceCharactersRange range = null;

            final Matcher detailMatcher = DETAIL_PATTERN.matcher(detail);
            if (detailMatcher.matches()) {
                // split location from string
                message = detailMatcher.group(1);
                final String location = detailMatcher.group(2);
                final String length = detailMatcher.group(3);
                range = new SourceCharactersRange(Integer.parseInt(location), Integer.parseInt(length));

            } else {
                // fall back to using all of detail as message
                message = detail;
            }

            return new SourceAnnotation(resultCode, message.trim(), range);
        }

        LOGGER.warn("Result line did not match expected source annotation format: {}" + line);
        return null;
    }
}
