package com.gatehill.apib.parser.model;

import com.gatehill.apib.parser.model.result.AstResult;

/**
 * @author pcornish
 */
public class ParsingResult<T> {
    private AstResult result;
    private T ast;

    public ParsingResult() {
    }

    public ParsingResult(AstResult result, T ast) {
        this.result = result;
        this.ast = ast;
    }

    public AstResult getResult() {
        return result;
    }

    public void setResult(AstResult result) {
        this.result = result;
    }

    public T getAst() {
        return ast;
    }

    public void setAst(T ast) {
        this.ast = ast;
    }
}
