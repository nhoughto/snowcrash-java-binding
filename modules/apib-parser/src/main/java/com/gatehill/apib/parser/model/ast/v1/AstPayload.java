package com.gatehill.apib.parser.model.ast.v1;

import java.util.Map;

/**
 * @author pcornish
 */
public abstract class AstPayload extends AstBase {
    /**
     * Payload-specific Parameters
     */
    private Map<String, AstParameter> parameters;

    /**
     * Payload-specific Headers
     */
    private Map<String, AstValueType> headers;

    /**
     * Body
     */
    private String body;

    /**
     * Schema
     */
    private String schema;

    public Map<String, AstValueType> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, AstValueType> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Map<String, AstParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, AstParameter> parameters) {
        this.parameters = parameters;
    }
}
